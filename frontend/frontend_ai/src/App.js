import React, {
  useState,
  useEffect,
} from 'react';
import sortBy from 'sort-by';
import Ballot from '@mui/icons-material/Ballot';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import CheckCircle from '@mui/icons-material/CheckCircle';
import ConfirmDialog from './ConfirmDialog';
import Container from '@mui/material/Container';
import DeleteIcon from '@mui/icons-material/Delete';
import DeleteForever from '@mui/icons-material/DeleteForever';
import Edit from '@mui/icons-material/Edit';
import Fab from '@mui/material/Fab';
import FormControlLabel from '@mui/material/FormControlLabel';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import MobileTearSheet from './MobileTearSheet';
import Tab from '@mui/material/Tab';
import TabPanel from '@mui/lab/TabPanel';
import TabList from '@mui/lab/TabList';
import TabContext from '@mui/lab/TabContext';
import TextField from '@mui/material/TextField';
import Splitscreen from '@mui/icons-material/Splitscreen';
import SearchAppBar from './SearchAppBar';
import If from './If';
import './App.css';

/**
 * @description Main App component.
 */
function App() {
    const initialValues = {
        items: [],
        taskIdCounter: 0,
        submitDisabled: true,
        slideIndex: 0,
        dialogOpen: false,
        removeMode: false,
    };
    const [initialState, setInitialState] = useState(initialValues);
    const [task, setTask] = useState(null);
    const [inputValue, setInputValue] = useState('');

    /**
     * Function called just after the App loads into the DOM.
     * Get any saved items and taskIdCounter from the local storage and setup state with it.
     */
    const componentWillMount = () => {
		const toDoListItems = window.localStorage.getItem('toDoListItems') || '[]';
		const taskIdCounter = parseInt(window.localStorage.getItem('taskIdCounter')) || 0;
		setInitialState(
			{   
                ...initialState,
				items: JSON.parse(toDoListItems),
				taskIdCounter: taskIdCounter,
			}
		);
	}

    useEffect(() => {
        componentWillMount();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

	/**
	 * @description Save items to local storage.
	 * @param {Object[]} items - Array of items/tasks to be saved.
	 */
	const updateLocalStorageItems = (items) => {
		window.localStorage.setItem('toDoListItems', JSON.stringify(items));
	};

	/**
	 * @description Update current taskId into local storage.
	 * @param {number} taskCounter - Id of the task to be saved at local storage.
	 */
	const updateTaskCounter = (taskCounter) => {
		window.localStorage.setItem('taskIdCounter', taskCounter);
	};
    
    /**
	 * @description Add task to the To Do list.
	 */
    const stateAddTaskComplete = () => {
        setTask(null);
        updateLocalStorageItems(initialState.items);
        updateTaskCounter(initialState.taskIdCounter);
    };
    
    /**
	 * @description Update items on localStorage.
	 */
    const stateTaskComplete = () => {
        updateLocalStorageItems(initialState.items);
    };

    /**
	 * @description scrollToTop.
	 */
    const scrollToTop = () => {
        window.scrollTo(0, 0);
    };

    /**
	 * @description Add task to the To Do list.
	 */
	const addTask = () => {
        const input = task;
        
		if (input === '' || input === null || input === undefined) return;
        setInputValue('');

        const { items } = initialState;
        const { taskIdCounter } = initialState;
        const taskId = parseInt(taskIdCounter)+1;
        const newTask = {
            id: taskId,
            title: input,
            status: 'To Do'
        };
        items.push(newTask);

		setInitialState({
            ...initialState,
            items: items.sort(sortBy('id')),
            submitDisabled: true,
            taskIdCounter: taskId,
        });
        stateAddTaskComplete();
	};

    /**
	 * @description Update task toggling between To Do/Done status.
	 * @param {Object} task - The task to be updated
	 */
	const handleUpdateTask = (task) => {
        const { items } = initialState;
        const filteredItems = items.filter( item => item.id !== task.id);
        task.status = (task.status === 'To Do') ? 'Done' : 'To Do';
        filteredItems.push(task);
		setInitialState({
            ...initialState,
            items: filteredItems.sort(sortBy('id'))
        });
        stateTaskComplete();
	};

	/**
	 * @description Remove task.
	 * @param {Object} task - The task to be removed.
	 */
	const handleRemoveTask = (task) => {
        const { items } = initialState;
        const filteredItems = items.filter( item => item.id !== task.id);
		setInitialState({
            ...initialState,
            items: filteredItems.sort(sortBy('id'))
		});
        stateTaskComplete();
	};

	/**
	 * @description Handle the Account Key TextField input change. It enable the submit button if field is not empty or
	 * disable it otherwise.
	 * @param {Object} event - On click event object
	 * @param {value} value - The task description
	 */
	const handleTextFieldChange = (value) => {
        setInputValue(value);
		if((value.length > 0) && initialState.submitDisabled){
			setInitialState({...initialState, submitDisabled: false});
		}
		else if((value.length === 0) && !initialState.submitDisabled){
			setInitialState({...initialState, submitDisabled: true});
		}
	};

	/**
	 * @description Handle the tab change.
	 * @param {number} value - The index of the Tab.
	 */
	const handleChange = (event, newValue) => {
		setInitialState({
            ...initialState,
			slideIndex: newValue,
		});
        scrollToTop();
	};

	/**
	 * @description Enable the remove task mode.
	 */
	const enableRemoveMode = () => {
		if (!initialState.removeMode) {
			setInitialState({...initialState, removeMode: true});
		}
	};

	/**
	 * @description Disable the remove task mode.
	 */
	const disableRemoveMode = () => {
		if (initialState.removeMode) {
			setInitialState({...initialState, removeMode: false});
		}
	};

    /**
	 * @description Remove all tasks from the App.
	 */
	const clearTasks = () => {
		handleDialogClose();
		setInitialState({...initialState, removeMode: false, items: [], dialogOpen: false});
        stateTaskComplete();
	};

	/**
	 * @description Open the clear tasks dialog.
	 */
	const handleDialogOpen = () => {
		setInitialState({...initialState, dialogOpen: true});
	};

	/**
	 * @description Close the clear task dialog.
	 */
	const handleDialogClose = () => {
		setInitialState({...initialState, dialogOpen: false});
	};

	/**
	 * @description Handle the enter key pressed under the add task input.
	 * @param {Object} e - Key press event
	 */
	const keyPress = (e) => {
		// If Enter key
		if(e.keyCode === 13){
			addTask();
		}
	};

    const { items = [] }  = initialState;
    const columns = [
        { title: 'To Do', items: items.filter(item => item.status === 'To Do'), icon: <Splitscreen />},
        { title: 'Done', items: items.filter(item => item.status === 'Done'), icon: <CheckCircle />},
        { title: 'All', items, icon: <Ballot />},
    ];

    return (
        <Container maxWidth="sm">
            <ConfirmDialog
                title="Clear All Tasks"
                message={'Are you sure you want to remove all tasks from the App?'}
                onCancel={handleDialogClose}
                onConfirm={clearTasks}
                open={initialState.dialogOpen}
                close={!initialState.dialogOpen}
            />
            <SearchAppBar
            />
            <div className="App-container">
                <div className="input-container">
                    <TextField
                        mt={2}
                        fullWidth
                        variant="standard"
                        label="Add Task"
                        value={inputValue}
                        disabled={initialState.removeMode}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            handleTextFieldChange(event.target.value);
                            setTask(event.target.value);
                        }}
                        onKeyDown={keyPress}
                    />
                    <Button
                        margin="normal"
                        variant="contained" 
                        size="medium"
                        onClick={addTask}
                        disabled={initialState.submitDisabled}>
                        Create
                    </Button>
                </div> 
                    <TabContext value={initialState.slideIndex.toString()}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <TabList onChange={handleChange} aria-label="To do tabs" centered>
                                {columns.map((column,index) => (
                                    <Tab 
                                    key={column.title}
                                    value={index.toString()}
                                    icon={column.icon}
                                    label={
                                        <div>
                                            {column.title} ({(column.title !== 'All') ? column.items.filter(item => item.status === column.title).length: items.length})
                                        </div>
                                    }
                                />
                            ))}
                            </TabList>
                        </Box>
                        <div className="app-lists">
                            <div>
                                {initialState.removeMode &&
                                    <div className="remove-mode">
                                        <Button
                                            onClick={handleDialogOpen}
                                        >
                                        Delete All Tasks
                                        </Button>
                                    </div>
                                }
                            </div>
                            {columns.map((column,index) => (
                                <TabPanel value={index.toString()} key={index}>
                                    <MobileTearSheet style={{pading: 10}}>
                                        <List>
                                            {column.items.map(item => (
                                                <ListItem
                                                    key={item.id+item.title}
                                                    onClick={() => (initialState.removeMode ? handleRemoveTask(item) : handleUpdateTask(item))}
                                                    secondaryAction={
                                                        <If test={initialState.removeMode}> 
                                                            <IconButton edge="end" aria-label="delete">
                                                                <DeleteIcon />
                                                            </IconButton>
                                                        </If>
                                                    }
                                                >
                                                    <FormControlLabel 
                                                        control={<Checkbox checked={item.status === 'Done'} />} 
                                                        label={item.title}
                                                        disabled={initialState.removeMode}
                                                        className={(item.status === 'Done') ? 'task-done': ''}
                                                    />
                                                </ListItem>
                                            ))}
                                        </List>
                                    </MobileTearSheet>
                                </TabPanel>
                            ))}
                        </div>
                    </TabContext>
            </div>
            <div className="enable-remove-mode">
                <If test={!initialState.removeMode}>
                    <Fab color="primary" aria-label="add" onClick={enableRemoveMode}>
                        <Edit />
                    </Fab>
                </If>
                <If test={initialState.removeMode}>
                    <Fab color="primary" aria-label="add" onClick={disableRemoveMode}>
                        <DeleteForever />
                    </Fab>
                </If>
            </div>
        </Container>
    );
}

export default App;