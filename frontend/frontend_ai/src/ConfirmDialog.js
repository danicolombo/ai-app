import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import './App.css';

/**
 * This object is used for type checking the props of the component.
 */
const propTypes = {
	title: PropTypes.string.isRequired,
	open: PropTypes.bool.isRequired,
	close: PropTypes.bool.isRequired,
	onCancel: PropTypes.func.isRequired,
	onConfirm: PropTypes.func.isRequired,
};

/**
 * This callback type is called `cancelCallback` and is displayed as a global symbol.
 *
 * @callback cancelCallback
 */

/**
 * This callback type is called `confirmCallback` and is displayed as a global symbol.
 *
 * @callback confirmCallback
 */

/**
 * @description Represents a material UI based Confirm Dialog.
 * @constructor
 * @param {Object} props - The props that were defined by the caller of this component.
 * @param {string} props.title - The message to be displayed by the dialog.
 * @param {boolean} props.open - The visibility of the dialog.
 * @param {boolean} props.close - The callback to execute when the user hits the cancel button.
 * @param {confirmCallback} props.onConfirm - The callback to execute when the user hits the confirm button.
 * @param {cancelCallback} props.onCancel - The callback to execute when the user hits the confirm button.
 */
function ConfirmDialog(props) {
    const closeDialog = () => {
        return props.close;
    }
	return (
        <Dialog
            open={Boolean(props.open)}
            onClose={closeDialog}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {props.title}
            </DialogTitle>
            <DialogActions>
            <Button onClick={props.onCancel}>Disagree</Button>
            <Button onClick={props.onConfirm} autoFocus>
                Agree
            </Button>
            </DialogActions>
        </Dialog>
	);
}

// Type checking the props of the component
ConfirmDialog.propTypes = propTypes;

export default ConfirmDialog;