# AI FRONTEND

This is a React frontend with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install` or  `yarn install`

Then:

### `npm start` or  `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
