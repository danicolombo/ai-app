# AI BACKEND

This is a Django backend.

## dev files

```
touch backend/backend_ai/backend_ai/.env
```

Add content:

```
OPENAI_API_KEY='personal-api-key'
DEBUG=True
```
