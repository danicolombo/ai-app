from django.apps import AppConfig
from langchain.llms import OpenAI
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain


class TodoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'todo'

    llm = OpenAI(temperature=0.9)  # model_name="text-davinci-003"
    text = "What would be a good company name for a company that makes colorful socks?"
    print(llm(text))

    prompt = PromptTemplate(
        input_variables=["product"],
        template="What is a good name for a company that makes {product}?",
    )
    print(prompt.format(product="colorful socks"))
    
    chain = LLMChain(llm=llm, prompt=prompt)
